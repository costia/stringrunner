var canvas = document.getElementById("renderCanvas");
var engine = new BABYLON.Engine(canvas, true);
BABYLON.OBJFileLoader.OPTIMIZE_WITH_UV = true;
laneColors=
  [new BABYLON.Color3(1.0, 0.2, 0.2),new BABYLON.Color3(1.0, 1.0, 0.2),
  new BABYLON.Color3(0.2, 0.8, 0.2),new BABYLON.Color3(0.2, 1.0, 1.0),
  new BABYLON.Color3(0.6, 0.2, 1.0),new BABYLON.Color3(1.0, 0.5, 0.2)]
lanes=[]
loadedMeshes=[]
gates=[]

score=0
health=100

 var createScene = function () {
    var scene = new BABYLON.Scene(engine);

    canvas2D = new BABYLON.ScreenSpaceCanvas2D(scene, {
        id: "ScreenCanvas",
        size: new BABYLON.Size(canvas.width, 50),
        //backgroundFill: "#4040408F",
        backgroundRoundRadius: 10,
        children: [
            hpText= new BABYLON.Text2D("", {
                id: "text",
                marginAlignment: "h: left, v:center",
                fontName: "20pt Arial",
            }),
            scoreText= new BABYLON.Text2D("", {
                id: "text2",
                marginAlignment: "h: right, v:center",
                fontName: "20pt Arial",
            })
        ]
    });
   

  var music = new BABYLON.Sound("Music", "looperman-l-1177757-0098769-jakebur4-spectre-lead-.ogg", scene, null, { loop: true, autoplay: true });
  music.setVolume(0.5)

    //window.addEventListener("keydown", handleKeyDown, false);
     scene.actionManager = new BABYLON.ActionManager(scene);
    scene.actionManager.registerAction(
      new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger,handleKeyDown ));
      
    scene.clearColor = new BABYLON.Color3(0, 0, 0);
    var camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 7, -10), scene);
    camera.setTarget(BABYLON.Vector3.Zero());

    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(5, 1, 0), scene);
    light.intensity = 0.7
    var light2 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(-5, -1, 0), scene);
    light2.intensity = 0.4

  for (var i=0;i<6;i++){
    lanes[i]= BABYLON.Mesh.CreatePlane("plane", 0.5, scene, false, BABYLON.Mesh.DEFAULTSIDE);
    lanes[i].position.x=i-2.5
    lanes[i].scaling.y=100
    lanes[i].scaling.x=0.5
    lanes[i].rotation.x=Math.PI/2
    var NewMaterial=new BABYLON.StandardMaterial("texture"+i, scene);
    NewMaterial.diffuseColor = laneColors[i]
    lanes[i].material = NewMaterial
  }

  var cylinder = BABYLON.Mesh.CreateCylinder("cylinder", 80, 10,20, 12, 1, scene, 
      false, BABYLON.Mesh.BACKSIDE);
  cylinder.rotation.x=Math.PI/2

    loader = new BABYLON.AssetsManager(scene);

    var textureTask = loader.addTextureTask("starTexture", "hipp2.png");
    textureTask.onSuccess = function(task) {
        starTexture = task.texture;
    }

    playerMesh = loader.addMeshTask("playerMesh", "", "", "untitled.obj");
    playerMesh.onSuccess=function(task){
    	task.loadedMeshes[0].rotation.y=Math.PI/2
    	task.loadedMeshes[0].scaling.x=0.3
    	task.loadedMeshes[0].scaling.y=0.3
    	task.loadedMeshes[0].scaling.z=0.3
    	task.loadedMeshes[0].position.z=-4.5
    	task.loadedMeshes[0].position.x=-0.5
    	loadedMeshes['playerMesh']=task.loadedMeshes[0]
    }

    loader.onFinish = function(tasks) {
    	var NewMaterial=new BABYLON.StandardMaterial("playerMat", scene);
    	var i=Math.floor(Math.random()*6)
        NewMaterial.diffuseColor = laneColors[i].multiply(new BABYLON.Color3(1.2,1.2,1.2));
        NewMaterial.useEmissiveAsIllumination = true;
        loadedMeshes['playerMesh'].material=NewMaterial
        loadedMeshes['playerMesh'].colorID=i;
        loadedMeshes['playerMesh'].currentLane=2;
        var animationWave = new BABYLON.Animation("sineWave", "position.y",
           60, BABYLON.Animation.ANIMATIONTYPE_FLOAT,BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
        var h=0.5;
        var keys = [{frame: 0,value: h},{frame: 30,value: h+0.1},{frame: 60,value: h},
          {frame: 90,value: h-0.1},{frame: 120,value: h}];
        animationWave.setKeys(keys);
        loadedMeshes['playerMesh'].animations.push(animationWave);
        scene.beginAnimation(loadedMeshes['playerMesh'], 0, 120, true);

        var NewMaterial=new BABYLON.StandardMaterial("tubeMat", scene);
        NewMaterial.diffuseTexture = starTexture
        cylinder.material = NewMaterial
        
        var animationTube = new BABYLON.Animation("uvAnim", "vOffset",
           60, BABYLON.Animation.ANIMATIONTYPE_FLOAT,BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE)
        var keys = [{frame: 0,value: 0},{frame: 120,value: 1}];
        animationTube.setKeys(keys);
        starTexture.animations.push(animationTube);
        scene.beginAnimation(starTexture, 0, 120, true);

        createGate(1);
    	engine.runRenderLoop(function() {
        	scene.render();
        	renderFunc();
    	});
	};
    loader.load();
    

    return scene;

  };  // End of createScene function

var scene = createScene();

function handleKeyDown(evt){
  if ((evt.sourceEvent.key=='a' || evt.sourceEvent.key=='ArrowLeft') && (loadedMeshes['playerMesh'].position.x>-2.5)){
    loadedMeshes['playerMesh'].position.x-=1;
    var music = new BABYLON.Sound("woop", "327752_distillerystudio_woop-transition-03.ogg", scene, null, { loop: false, autoplay: true });    
    music.setVolume(0.5)
  }
  if ((evt.sourceEvent.key=='d' || evt.sourceEvent.key=='ArrowRight') && (loadedMeshes['playerMesh'].position.x<2.5)){
    loadedMeshes['playerMesh'].position.x+=1;
    var music = new BABYLON.Sound("woop", "327752_distillerystudio_woop-transition-03.ogg", scene, null, { loop: false, autoplay: true });    
    music.setVolume(0.5)
  }
  loadedMeshes['playerMesh'].currentLane=loadedMeshes['playerMesh'].position.x+2.5
}

function renderFunc(){
 if (loadedMeshes['playerMesh'].currentLane==loadedMeshes['playerMesh'].colorID){
  score+=1;
 }else{
   health-=0.15;
 }
 if (health<0) health=0;
 hpText.text="HP: "+Math.floor(health)+"%"
 scoreText.text="Score: "+Math.floor(score)
 if (health<=0){
    canvas2D = new BABYLON.ScreenSpaceCanvas2D(scene, {
        id: "ScreenCanvas2",
        size: new BABYLON.Size(canvas.width, canvas.height),
        children: [
            new BABYLON.Text2D("Game Over", {
                id: "text",
                marginAlignment: "h: center, v:center",
                fontName: "40pt Arial",
            })
        ]
    });
    scene.render()
  engine.stopRenderLoop()
 }
}

// window.addEventListener("resize", function () {
//  engine.resize();
// });

function createGate(speed){
    var gate=new BABYLON.Mesh.CreateTorus("torus", 7, 0.3, 20, scene, false, BABYLON.Mesh.DEFAULTSIDE);
    gate.rotation.x=Math.PI/2
    gate.position.y=1
    gate.position.z=100
    var NewMaterial=new BABYLON.StandardMaterial("gateMat", scene);
    var i=Math.floor(Math.random()*6)
    gate.colorID=i;
    NewMaterial.diffuseColor = laneColors[i];
    NewMaterial.emissiveColor = new BABYLON.Color3(0.25, 0.25, 0.25);
    gate.material = NewMaterial

    var animationGate = new BABYLON.Animation("position.z", "position.z",
       60, BABYLON.Animation.ANIMATIONTYPE_FLOAT,
       BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT)
    var keys = [{frame: 0,value: 20},{frame: 120,value: -10}];
    animationGate.setKeys(keys);
    gate.animations.push(animationGate);
    scene.beginAnimation(gate, 0, 120, false,Math.sqrt(speed),function(){
      var music = new BABYLON.Sound("Music", "128586_corsica-s_bhab.ogg", scene, null, { loop: false, autoplay: true });
      
        loadedMeshes['playerMesh'].colorID=gate.colorID;
        scene.getMaterialByID("playerMat").diffuseColor=gate.material.diffuseColor
        gate.dispose();
        createGate(speed+0.2);
      });

}